from vidark import VidArk
from vidark_sql import gen_q_videos_by_tags

q1 = gen_q_videos_by_tags(inc=["music","bears","animals"], exc=["people","machines"])
q2 = gen_q_videos_by_tags(inc=["music","bears","animals"])
q3 = gen_q_videos_by_tags(exc=["people","machines"])

va = VidArk()


vts = """


select
  v.id as id,
  v.title as title,
  group_concat(t.name) as tags
from videos v
inner join video_tags vt on v.id = vt.video_id
inner join tags t on vt.tag_id = t.id
group by v.id

;


"""

vbt = """


select
  v.id as id,
  v.title as title,
  group_concat(t.name) as tags
from videos v
inner join video_tags vt on v.id = vt.video_id
inner join tags t on vt.tag_id = t.id
group by v.id
having
SUM(CASE WHEN t.name = 'science' THEN 1 ELSE 0 END) > 0
AND
SUM(CASE WHEN t.name = 'technology' THEN 1 ELSE 0 END) > 0
AND
SUM(CASE WHEN t.name = 'minecraft' THEN 1 ELSE 0 END) > 0

;


"""

def runq(q):
    cur = va._q(q)
    for r in cur:
        print(dict(r))



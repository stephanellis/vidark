# main vidpad lib


import os
import json
import wget
import sqlite3
import datetime
from glob import glob
from uuid import uuid4
from vidark_sql import *
from youtube_dl import YoutubeDL


_settings_defaults = dict(
    video_dir='/home/stephan/Videos/vidpad',
    content_base_url='http://localhost:8000',
)



def _uh():
    return uuid4().hex


class Object():
    pass


class VidArk():
    """
    the main system class
    """


    def ydl_p_hook(self, d):
        pass


    @property
    def _settings(self):
        o = Object()
        for k in _settings_defaults.keys():
            o.__dict__[k] = _settings_defaults[k]
            setattr(o, k, _settings_defaults[k])
        for r in self._q(Q_ALL_SETTINGS):
            o.__dict__[r["name"]] = r["val"]
            setattr(o, r["name"], r["val"])
        return o


    def __init__(self, db='db.sqlite', debug=False, progress_hooks=[]):
        self.db = db
        self.debug = debug
        self._ydl_opts = {
            'progress_hooks': [self.ydl_p_hook],
            'format':'mp4',
            #'outtmpl': self._settings.video_dir + '/%(title)s.%(ext)s',
        }
        for p in progress_hooks:
            self._ydl_opts['progress_hooks'].append(p)
        self.conn = sqlite3.connect(self.db)
        self.conn.row_factory = sqlite3.Row


    @property
    def _video_dir(self):
        vd = self.settings()['video_dir']
        # check if the dir exists, if not, make it
        if not os.path.isdir(vd):
            os.mkdir(vd)
        return vd


    def get_video_byid(self, vid):
        v = self._q("select * from videos where id = ?;", (vid,)).fetchone()
        return v


    def delete_video(self, video_id):
        cur = self._c()
        cur.execute("delete from video_tags where video_id = ?;", (video_id,))
        cur.execute("delete from videos where id = ?;", (video_id,))
        self.conn.commit()
        files = glob("{}/{}*".format(self._settings.video_dir, video_id))
        for f in files:
            os.remove(f)
        return True


    def get_video_tags(self, video_id):
        return self._q("select tags.id, tags.name from video_tags inner join tags on video_tags.tag_id = tags.id where video_tags.video_id = ?;", (video_id,))


    def all_videos(self, level=0):
        return self._q(Q_ALL_VIDEOS, (level,))


    def dump_videos_asc(self, level=100):
        return self._q(Q_ALL_VIDEOS_ASC, (level,))


    def videos_by_tags(self, inc=[], exc=[], level=0):
        params = gen_q_videos_by_tags(inc=inc, exc=exc, level=level)
        q = params.pop(0)
        return self._q(q, params)


    def all_tags(self):
        return self._q(Q_ALL_TAGS)


    def get_tag(self, name):
        return self._q("select * from tags where name = ?", (name,)).fetchone()


    def get_or_create_tag(self, name):
        name = name.lower()
        t = self.get_tag(name)
        if t is not None:
            return t
        else:
            self._q("insert into tags (name) values (?);", (name,))
            self.conn.commit()
            return self.get_tag(name)


    def get_user(self, email):
        return self._q("select * from users where email = ?", (email,)).fetchone()


    def login(self, email, password):
        return self.get_user(email)


    def _c(self):
        return self.conn.cursor()


    def _q(self, *args):
        cur = self.conn.cursor()
        return cur.execute(*args)


    def ydl_opts(self):
        return self._ydl_opts


    def download(self, url, tags=[], level=0, date_added=None):
        vh = _uh() # get a unique id for the video
        output_file = "{}/{}.mp4".format(self._video_dir, vh)
        thumb_file = "{}/{}.thumb".format(self._video_dir, vh)
        ydl = YoutubeDL(self.ydl_opts())
        vinfo = ydl.extract_info(url, download=False)
        if 'thumbnails' in vinfo:
            if len(vinfo['thumbnails']) > 0:
                wget.download(vinfo['thumbnails'][0]['url'], thumb_file)

        ydl.params['outtmpl'] = "{}/{}".format(self._settings.video_dir, vh)
        ydl.download([url])
        vid_record = (
            vh,
            url,
            "OK",
            vinfo["title"],
            vinfo["description"],
            vinfo["duration"],
            date_added or datetime.datetime.now(),
            level
        )
        cur = self._c()
        cur.execute('insert into videos (id, url, status, title, desc, duration, date_added, level) values (?, ?, ?, ?, ?, ?, ?, ?);', vid_record)
        self.conn.commit()
        if len(tags) > 0:
            cur = self._c()
            video_tags = []
            for t in tags:
                tag = self.get_or_create_tag(t)
                video_tags.append((vh, tag["id"],))
            cur.executemany("insert into video_tags (video_id, tag_id) values (?,?);", video_tags)
            self.conn.commit()
        return ydl


    def settings(self):
        d = dict()
        for r in self._q(Q_ALL_SETTINGS):
            d[r["name"]] = r["val"]
        return d



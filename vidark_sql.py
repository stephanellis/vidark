# queries used in the vidark lib


Q_SCHEMA = """
CREATE TABLE settings (name primary key, val);
CREATE TABLE videos (id primary key, url, title, status, date_added, duration, desc, level integer);
CREATE TABLE tags (id integer primary key autoincrement, name unique);
CREATE TABLE sqlite_sequence(name,seq);
CREATE TABLE video_tags (video_id, tag_id integer, foreign key (video_id) REFERENCES videos(id), foreign key (tag_id) REFERENCES tags(id), primary key (video_id, tag_id));
CREATE TABLE users (id integer PRIMARY KEY AUTOINCREMENT, email, name, password, level);
"""


Q_ALL_SETTINGS = "select * from settings;"
Q_ALL_VIDEOS = """
SELECT
  v.id as id,
  v.url as url,
  v.title as title,
  v.status as status,
  v.date_added as date_added,
  v.duration as duration,
  v.desc as desc,
  v.level as level,
  GROUP_CONCAT(t.name) as tags
FROM videos v
LEFT JOIN video_tags vt ON v.id = vt.video_id
LEFT JOIN tags t ON vt.tag_id = t.id
WHERE level <= ?
GROUP BY v.id
ORDER BY date_added DESC
;

"""
Q_ALL_VIDEOS_ASC = """
SELECT
  v.id as id,
  v.url as url,
  v.title as title,
  v.status as status,
  v.date_added as date_added,
  v.duration as duration,
  v.desc as desc,
  v.level as level,
  GROUP_CONCAT(t.name) as tags
FROM videos v
LEFT JOIN video_tags vt ON v.id = vt.video_id
LEFT JOIN tags t ON vt.tag_id = t.id
WHERE level <= ?
GROUP BY v.id
ORDER BY date_added ASC
;

"""
Q_ALL_TAGS = "select * from tags order by name asc;"



# this is a fragment, used in gen_q_videos_by_tags below
_Q_VIDEOS_BY_TAGS_BASE = """

SELECT
  v.id as id,
  v.url as url,
  v.title as title,
  v.status as status,
  v.date_added as date_added,
  v.duration as duration,
  v.desc as desc,
  v.level as level,
  GROUP_CONCAT(t.name) as tags
FROM videos v
INNER JOIN video_tags vt ON v.id = vt.video_id
INNER JOIN tags t ON vt.tag_id = t.id
WHERE v.level <= ?
GROUP BY v.id
HAVING

"""

_Q_VIDEOS_BY_TAG_INC = "SUM(CASE WHEN t.name = ? THEN 1 ELSE 0 END) > 0"
_Q_VIDEOS_BY_TAG_EXC = "SUM(CASE WHEN t.name = ? THEN 1 ELSE 0 END) = 0"

def gen_q_videos_by_tags(inc=[], exc=[], level=0):
    frags = []
    params = []
    params.append(level)
    for t in inc:
        frags.append(_Q_VIDEOS_BY_TAG_INC)
        params.append(t)
    for t in exc:
        frags.append(_Q_VIDEOS_BY_TAG_EXC)
        params.append(t)
    q = _Q_VIDEOS_BY_TAGS_BASE + "\nAND\n".join(frags) + " ORDER BY v.date_added DESC;"
    params.insert(0, q)
    return params



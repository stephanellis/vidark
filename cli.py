#! /usr/bin/env pythong


import json
import click
from web import run_dev_server
from vidark import VidArk


def ydl_p_hook(d):
    click.echo(json.dumps(d, indent=2))


@click.group()
@click.pass_context
def cli(ctx):
    pass


@cli.command()
@click.argument("url")
@click.option("--tags", help="command separated list of tags", default="")
@click.option("--level", help="set video access level", default=50)
@click.option("--date_added", help="set date to value instead of now", default=None)
@click.pass_context
def download(ctx, url, tags, level, date_added):
    v = VidArk()
    tagslist = [ x.lower() for x in tags.strip().split(",") if x != "" ]
    v.download(url, tags=tagslist, level=level, date_added=date_added)


@cli.command()
@click.pass_context
def dumpvideos(ctx):
    v = VidArk()
    videos = v.dump_videos_asc()
    for v in videos:
        print("python cli.py download --tags {} --level {} --date_added =\"{}\" {}".format(v["tags"], v["level"], v["date_added"], v["url"]))


@cli.command()
def dev():
    run_dev_server()


@cli.command()
def settings():
    v = VidArk()
    click.echo(json.dumps(v._settings.__dict__, indent=2))


if __name__ == '__main__':
    cli()



#! /usr/bin/env python


from flask import Flask, session, g, render_template, send_from_directory
from flask import render_template, redirect, url_for, send_from_directory
from flask import request
from vidark import VidArk


app = Flask(__name__)
app.secret_key = 'sdlkn2@#F@Fksdf!#'


@app.before_request
def ctx_tags():
    g.va = VidArk()
    g.tags = g.va.all_tags()
    if 'level' not in session:
        session['level'] = 0
    return None


@app.route("/")
def index():
    level = 0
    if 'level' in session:
        level = session['level']
    inc = list()
    exc = list()
    videos = list()
    if 'inc' in session or 'exc' in session:
        if 'inc' in session:    
            inc = session['inc']
        if 'exc' in session:
            exc = sessopm['exc']
        videos = g.va.videos_by_tags(inc=inc, exc=exc, level=level)
    else:
        videos = g.va.all_videos(level=level)
    return render_template("index.html", videos=videos)


@app.route("/content/<path:filename>")
def content_static(filename):
    return send_from_directory(g.va._settings.video_dir, filename)


@app.route("/watch/<id>")
def watch(id):
    video = g.va.get_video_byid(id)
    videotags = [ t["name"] for t in g.va.get_video_tags(id) ]
    return render_template("watch.html", v=video, videotags=videotags, watch=True)


@app.route("/video/<id>/addtag/<name>")
def video_addtag(id, name):
    t = g.va.get_or_create_tag(name)
    cur = g.va._c()
    cur.execute("insert into video_tags (video_id, tag_id) values (?, ?);", (id, t["id"],))
    g.va.conn.commit()
    return redirect(request.referrer)


@app.route("/video/<id>/deltag/<name>")
def video_deltag(id, name):
    t = g.va.get_or_create_tag(name)
    cur = g.va._c()
    cur.execute("delete from video_tags where video_id = ? and tag_id = ?;", (id, t["id"],))
    g.va.conn.commit()
    return redirect(request.referrer)


@app.route("/video/delete/<id>")
def video_delete(id):
    g.va.delete_video(id)
    return redirect(url_for('index'))


@app.route("/tags/include/<name>")
def inc_tag(name):
    if 'inc' not in session:
        session['inc'] = list()
    if name not in session['inc']:
        print("adding tag {}".format(name))
        session['inc'].append(name)
        print("inc is now {}".format(session['inc']))
    session.modified = True
    return redirect(request.referrer)


@app.route("/tags/clear")
def clear_tags():
    if 'inc' in session:
        session.pop("inc")
    if 'exc' in session:
        session.pop("exc")
    return redirect(request.referrer)


@app.route("/ctrl/setlevel/<level>")
def setlevel(level):
    session["level"] = level
    return redirect(url_for('index'))


@app.route("/login/<email>")
def login(email):
    u = g.va.login(email, "")
    if u is not None:
        session["level"] = u["level"]
        session["u"] = dict(u)
    return redirect(url_for('index'))


@app.route("/logout")
def logout():
    if 'u' in session:
        session.pop('u')
    session["level"] = 0
    return redirect(url_for('index'))


def run_dev_server():
    app.run(debug=True, host="0.0.0.0", port=8000)


